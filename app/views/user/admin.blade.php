<div class="container">
    <h2>Dashboard</h2>

    <div class="row">
        @if($user->hasAccess('create content') || $user->hasAccess('administer content') || $user->hasAccess('administer categories') )
        <div class="col col-md-6">
            <h2>Content</h2>
            <ul>
                @if($user->hasAccess('create content'))
                <li><a href="/admin/content/create">Create content</a></li>
                @endif
                @if($user->hasAccess('administer content'))
                <li><a href="/admin/content">Manage content</a></li>
                @endif
                @if($user->hasAccess('administer categories'))
                <li><a href="/admin/category">Manage categories</a></li>
                @endif
            </ul>
        </div>
        @endif
        @if($user->hasAccess('administer settings'))
        <div class="col col-md-6">
            <h2>Settings</h2>
            <ul>
                <li><a href="/admin/site-settings">Site settings</a></li>
                <li><a href="/admin/error-pages">Error pages</a></li>
            </ul>
        </div>
        @endif
    </div>
    <div class="row">
    	@if($user->hasAccess('administer users'))
        <div class="col col-md-6">
            <h2>User management</h2>
            <h3>Users</h3>
            <ul>
                <li><a href="/admin/users">List users</a></li>
                <li><a href="/admin/users/add" >Add a user</a></li>
            </ul>

            <h3>Groups</h3>
            <ul>
                <li><a href="/admin/groups">List groups</a></li>
                <li><a href="/admin/groups/add" >Add a group</a></li>
            </ul>
        </div>
        @endif
        @if($user->hasAccess('administer menus'))
        <div class="col col-md-6">
            <h2>Menus</h2>
            <ul>
                <li><a href="/admin/menu/settings">Menu settings</a></li>
                <li><a href="/admin/menu">List menus</a></li>
                <li><a href="/admin/menu/add">Add menu</a></li>
            </ul>
        </div>
        @endif
    </div>

</div>


<?php

class Term extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'terms';
	public static $rules = array(
        'name'=>'required',
    );

	public function category()
    {
        return $this->belongsTo('Category');
    }

}

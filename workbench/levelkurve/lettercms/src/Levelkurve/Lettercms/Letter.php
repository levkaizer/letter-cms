<?php namespace LevelKurve\Lettercms;

class Letter {
	public $config = array();
	public $user;
	public $menu;
	public $userMenu;
	public $userLoggedInMenu;

	/**
	 * Return the global config array anonymous
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * Return the current logged in User.
	 * Wraps around the Sentry User Object, which is set in the Service Provider.
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Simple check to see if the user is logged in.
	 */
	public function isAnonymous(\Cartalyst\Sentry\Users\Eloquent\User $user = null) {
		if(!isset($user)) {
			$user = $this->user;
		}
		if($user->id > 0) {
			return false;
		}
		return true;
	}

	public function setMenu($theMenu) {
		$this->menu = $theMenu;
	}

	public function menu() {
		return $this->menu;
	}

	public function setUserMenu($theMenu) {
		$this->userMenu = $theMenu;
	}

	public function userMenu() {
	   return $this->userMenu;
	}

	public function setUserLoggedInMenu($theMenu) {
		$this->userLoggedInMenu = $theMenu;
	}

	public function userLoggedInMenu() {
	   return $this->userLoggedInMenu;
	}
}


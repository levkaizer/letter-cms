<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

Route::filter('auth.admin.menu', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('create content')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

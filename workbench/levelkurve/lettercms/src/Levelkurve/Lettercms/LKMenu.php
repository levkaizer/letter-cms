<?php namespace LevelKurve\Lettercms;
use \Levelkurve\Lettercms\Models\MenuItem as MenuItem;

class LKMenu {
    public $config = array();

    public function __construct() {
        $this->configure(array());
    }

    public function configure($array = array()) {
        $defaults = array(
            'ul_wrap'       => "<div class=\"collapse navbar-collapse\">\n",
            'ul_end_wrap'   => '</div>',
            'ul_class'       => 'nav navbar-nav',
            'li_class'       => 'menu-item',
            'include_ul'     => true,
            // sub nav
            'ul_sub_class' => 'sub-nav dropdown-menu',
        );

        $this->config = array_merge($defaults, $array);
    }

	public function about() {
		return "Menu!";
	}

	public function build($id = null, $depth = 0) {
	    if(isset($id)) {
	        $menuItems = MenuItem::where('menu_id', '=', e($id))
	        	->where('parent', '=', 0)
	            ->where('active', '=', 1)
	            ->orderBy('weight', 'asc')
	            ->get();

	        if($depth > 0) {
	        	foreach($menuItems as $m) {
	        		$children = MenuItem::where('menu_id', '=', e($id))
	        			->where('parent', '=', $m->id)
			            ->where('active', '=', 1)
			            ->orderBy('weight', 'asc')
			            ->get();
			        if(sizeof($children)) {
			        	$m->children = $children;
			        }

	        	}
	        }

	        return $menuItems;
	    }
	    return false;
	}

	public function render($id = null) {
	    if(isset($id)) {
	        // walk the array and build the HTML.
	        $menuItems = $this->build($id, 1);
	        if(isset($menuItems)) {
	            if(sizeof($menuItems)) {
	                $end = false;
	                $html = '';
	                if(isset($this->config['ul_wrap']) && isset($this->config['ul_end_wrap'])) {
	                    $end = true;
	                    $html .= $this->config['ul_wrap'];
	                }

	                $html .= "\t";

	                if(isset($this->config['ul_class'])) {
	                    $html .= '<ul id="menu-' . $id . '" class="' . $this->config['ul_class'] . '">';
	                }
	                else {
	                    if($this->config['include_ul']) {
	                        $html .= '<ul id="menu-' . $id . '">';
	                    }
	                }

	                $html .= "\n";


	                foreach($menuItems as $mi) {
	                    $html .= "\t\t";
	                    $class = false;
	                    if(isset($mi->children)) {
	                    	$class = ' dropdown';
	                    }
	                    if(isset($this->config['li_class'])) {
	                        $html .= '<li class="' . $this->config['li_class'] . $class.'">';
	                    }
	                    else {
	                        $html .= '<li>';
	                    }

	                    $html .= "\n";

	                    $html .= "\t\t\t";

	                    $toggle = ($class) ? 'class="dropdown-toggle" data-toggle="dropdown"' : '' ;
	                    $html .= '<a href="' . $mi->path . '" '. $toggle .'>';
	                    $html .= e( $mi->title );
	                    if(strlen($toggle)) {
	                    	$html .= ' <b class="caret"></b>';
	                    }
	                    $html .= '</a>';

	                    $html .= "\n\t\t";

	                    // check for multi level menu
	                    if(isset($mi->children)) {
	                    	$html .= "\t\t\t";
	                    	$html .= '<ul class="' . $this->config['ul_sub_class'] . '">';
	                    	foreach($mi->children as $m) {
	                    		$html .= "\n\t\t\t\t\t\t";
	                    		$html .= "<li>";
	                    		$html .= "\n\t\t\t\t\t\t\t";
	                    		$html .= '<a href="'. $m->path . '"> ' . e($m->title) . ' </a>';
	                    		$html .= "\n\t\t\t\t\t\t";
	                    		$html .= "</li>";
	                    		$html .= "\n";
	                    	}
	                    	$html .= "\t\t";
	                    	$html .= "</ul>";
	                    	$html .= "\n\t\t";
	                    }

	                    $html .= '</li>';

	                    $html .= "\n";
	                }

	                $html .= "\t";
	                if($this->config['include_ul']) {
	                    $html .= '</ul>';
	                }

	                $html .= "\n";

	                if($end) {
	                    $html .= $this->config['ul_end_wrap'];
	                }

	                $html .= "\n";
	                return $html;
	            }
	        }
	    }
	    return false;
	}



}


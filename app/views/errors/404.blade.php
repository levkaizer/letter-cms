<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Letter CMS</title>
    <link media="all" type="text/css" rel="stylesheet" href="http://letter/packages/bootstrap/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://letter/css/main.css">

    <script src="http://letter/js/jquery-1.11.0.min.js"></script>
    <script src="http://letter/packages/bootstrap/js/bootstrap.min.js"></script>
  </head>

  <body>

   <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                      <a class="navbar-brand" href="/admin">Letter CMS</a>
                  </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
                      </ul>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/user/logout">Logout</a></li>
            </ul>
          </div>
                  </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
          </div>

   <div class="container">
    <h2>404 Page not Found</h2>
    {{{ $letter->config['not_found_msg'] }}}
   </div>

   <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand">&copy; 2014 LevelKurve</a>
        </div>
      </div>
   </nav>
  </body>
</html>



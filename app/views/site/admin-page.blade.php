<div class="container">
    <h2>{{{ $page_title }}}</h2>

    {{ $content }}

</div>

<!-- Modal -->
@if( $modal )
<div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal-admin-title">{{{ $modal_title }}}</h4>
      </div>
      <div class="modal-body">
        {{ $modal_content }}
      </div>
    </div>
  </div>
</div>
@endif

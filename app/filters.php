<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (! Sentry::guest()) return Redirect::guest('user/login')->with('message', 'You are not logged in.');
});

Route::filter('auth.admin', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if(!$user->hasAccess('access administration pages')) {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

Route::filter('auth.admin.users', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('administer users')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

Route::filter('auth.admin.users.permissions', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('administer users') && $user->hasAccess('administer permissions')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

Route::filter('auth.admin.content.create', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('create content')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

Route::filter('auth.admin.content.manage', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('administer content')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});

Route::filter('auth.admin.category', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('administer categories')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});
/*
Route::filter('auth.admin.term', function()
{
	if (! Sentry::check())
	{
		return Redirect::guest('user/login')->with('message', 'You are not logged in.');
	}

	$user = Letter::getUser();
	if($user->hasAccess('access administration pages') && $user->hasAccess('create content')) {
	}
	else {
		return Redirect::guest('user/login')->with('message', 'You are not allowed in.');
	}

});
*/

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('user/login');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

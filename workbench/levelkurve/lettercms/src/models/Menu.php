<?php namespace Levelkurve\Lettercms\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Menu extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'menu';
	protected $fillable = array('name');
	public static $rules = array(
        'name'=>'required|unique:menu',
    );
    
    public function items() {
    	return $this->hasMany('\Levelkurve\Lettercms\Models\MenuItem');
    }


}

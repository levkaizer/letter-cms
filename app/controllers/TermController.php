<?php

class TermController extends BaseController {
    protected $layout = "layouts.main";

    public function getIndex() {
    }
    
    public function getAdd(Category $category) {
    	$form  = Form::open(array('url' => '/admin/category/'.$category->id.'/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('name', 'Term name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Enter a term name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add Category',
            'content' => $content,
            'modal' => false,
        ));
    }
    
    public function postAdd(Category $category) {
    	$term = new Term();
	    $term->name = Input::get('name');
	    $term->weight = 0;
	    $term->category_id = $category->id;

	    $validator = Validator::make(Input::all(), Term::$rules);
	    if($validator->passes()) {
	        $term->save();
	        return Redirect::to('admin')->with('message', 'New term added.');
	    }

	    return Redirect::back()->withErrors($validator)->withInput();
    }
    
    public function getEdit(Term $term)
    {
    	$form  = Form::open(array('url' => '/admin/category/term/'.$term->id.'/edit', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('name', 'Term name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('name', $term->name, array('class' => 'form-control', 'placeholder' => 'Enter a term name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add Category',
            'content' => $content,
            'modal' => false,
        ));
    }
    
    public function postEdit(Term $term) {
	    $term->name = Input::get('name');
	    $validator = Validator::make(Input::all(), Term::$rules);
	    if($validator->passes()) {
	        $term->save();
	        return Redirect::to('admin/category')->with('message', 'Category updated.');
	    }

	    return Redirect::back()->withErrors($validator)->withInput();
	}

	public function getDelete(Term $term) {
	    $term->delete();
	    return Redirect::to('/admin/category')->with('message', 'Category has been removed.');
	}
}
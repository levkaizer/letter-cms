/**
 * Function to allow dynamic refresh of modal windows.
 * Specifically, called on the permissions page.
 */
 var group_id = 0;
$(function(){
    $('#adminModal').on('show.bs.modal', function (e) {
       $('.modal-body').html('');
       $('#modal-admin-title').text('Loading...');
       var buttonId = $(e.relatedTarget).prop('id');
       var id = $(e.relatedTarget).prop('id').split('-');
       group_id = id[ id.length-1 ];
       $.ajax({
         url: '/admin/groups/'+group_id+'/permissions',
         dataType: 'html',

       }).done(function(data){
         $('.modal-body').html(data);
         $('#modal-admin-title').text( 'Permissions for ' + $('#group-title-'+group_id).text() );
       }).fail(function(){
        alert('There was an error communicating with the server.  Please try again later.');
       });
    });
});

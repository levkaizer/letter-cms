<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses' => 'HomeController@getIndex'));

Route::get('user/register', array('uses' => 'UserController@getRegister'));
Route::post('user/register', array('uses' => 'UserController@postRegister'));


Route::get('user/login', array('uses' => 'UserController@getLogin'));
Route::post('user/login', array('uses' => 'UserController@postLogin'));

Route::get('user/logout', array('uses' => 'UserController@getLogout'));

Route::group(array('before' => 'auth.admin', 'prefix' => 'admin'), function() {
	Route::get('/', array('uses' => 'AdminController@getIndex'));
	Route::get('site-settings', array('uses' => 'AdminController@getSiteSettings'));
	Route::post('site-settings', array('uses' => 'AdminController@postSiteSettings'));

	Route::get('error-pages', array('uses' => 'AdminController@getErrorPages'));
	Route::post('error-pages', array('uses' => 'AdminController@postErrorPages'));
});

Route::group(array('before' => 'auth.admin.users', 'prefix' => 'admin/users'), function() {
	Route::model('user', 'Cartalyst\Sentry\Users\Eloquent\User');

	Route::get('/', array('uses' => 'UserController@getAdminListUsers'));

	Route::get('add', array('uses' => 'UserController@getAdminAdd'));
	Route::post('add', array('uses' => 'UserController@postAdminAdd'));

	Route::get('{user}/edit', array('uses' => 'UserController@getAdminEdit'));
	Route::post('{user}/edit', array('uses' => 'UserController@postAdminEdit'));

	Route::get('{user}/delete', array('uses' => 'UserController@getAdminDelete'));

});

Route::group(array('before' => 'auth.admin.users.permissions', 'prefix' => 'admin/groups'), function() {
	Route::model('group', 'Cartalyst\Sentry\Groups\Eloquent\Group');

	Route::get('/', array('uses' => 'GroupController@getIndex'));

	Route::get('add', array('uses' => 'GroupController@getAdd'));
	Route::post('add', array('uses' => 'GroupController@postAdd'));

	Route::get('{group}/edit', array('uses' => 'GroupController@getEdit'));
	Route::post('{group}/edit', array('uses' => 'GroupController@postEdit'));

	Route::get('{group}/delete', array('uses' => 'GroupController@getDelete'));

	Route::get('{group}/permissions/edit', array('uses' => 'GroupController@getEditPermissionsForGroup'));
	Route::post('{group}/permissions/edit', array('uses' => 'GroupController@postEditPermissionsForGroup'));

	// AJAX
	Route::get('{group}/permissions', array('uses' => 'GroupController@getPermissionsForGroup'));

});

Route::group(array('before' => 'auth.admin.content.create', 'prefix' => 'admin/content'), function() {
	Route::model('content', 'Content');

	Route::get('/', array('as' => 'admin.content', 'uses' => 'ContentController@getIndex'));

	Route::get('create', array('as' => 'admin.content.getCreate', 'uses' => 'ContentController@getCreate'));
	Route::post('create', array('as' => 'admin.content.postCreate','uses' => 'ContentController@postCreate'));
});

Route::group(array('before' => 'auth.admin.content.manage', 'prefix' => 'admin/content'), function() {
    Route::model('content', 'Content');

    Route::get('{content}/edit', array('as' => 'admin.content.getEdit', 'uses' => 'ContentController@getEdit'));
	Route::post('{content}/edit', array('as' => 'admin.content.postEdit', 'uses' => 'ContentController@postEdit'));

	Route::get('{content}/delete', array('uses' => 'ContentController@getDelete'));
});

Route::group(array('prefix' => 'content'), function() {
    Route::model('content', 'Content');
    Route::get('/{content}', array('as' => 'content', 'uses' => 'ContentController@getView'))->where('id', '\d+');

});

Route::group(array('before' => 'auth.admin.category', 'prefix' => 'admin/category'), function() {
	Route::model('category', 'Category');

	Route::get('/', array('as' => 'admin.category', 'uses' => 'CategoryController@getIndex'));

    Route::get('/add', array('as' => 'admin.category.getAdd', 'uses' => 'CategoryController@getAdd'));
    Route::post('/add', array('as' => 'admin.category.postAdd', 'uses' => 'CategoryController@postAdd'));

    Route::get('{category}/edit', array('as' => 'admin.category.getEdit', 'uses' => 'CategoryController@getEdit'));
    Route::post('{category}/edit', array('as' => 'admin.category.postEdit', 'uses' => 'CategoryController@postEdit'));

    Route::get('{category}/delete', array('as' => 'admin.category.getDelete', 'uses' => 'CategoryController@getDelete'));

    Route::get('{category}', array('as' => 'admin.category.getList', 'uses' => 'CategoryController@getList'));

});

Route::group(array('prefix' => 'category'), function() {
    Route::model('category', 'Category');
    Route::get('/{category}', array('as' => 'category', 'uses' => 'CategoryController@getTaggedPages'))->where('id', '\d+');

});

Route::group(array('before' => 'auth.admin.term', 'prefix' => 'admin/category'), function() {
	Route::model('category', 'Category');
	Route::model('term', 'Term');

    Route::get('{category}/add', array('as' => 'admin.term.getAdd', 'uses' => 'TermController@getAdd'));
    Route::post('{category}/add', array('as' => 'admin.term.postAdd', 'uses' => 'TermController@postAdd'));

    Route::get('term/{term}/edit', array('as' => 'admin.term.getEdit', 'uses' => 'TermController@getEdit'));
    Route::post('term/{term}/edit', array('as' => 'admin.term.postEdit', 'uses' => 'TermController@postEdit'));

    Route::get('term/{term}/delete', array('as' => 'admin.term.getDelete', 'uses' => 'TermController@getDelete'));

});

Route::any('{all}', function($uri)
{
    // need to look up content by path
    try
    {
    	$path = Path::where('path', '=', e($uri))->firstOrFail();
    	$content = $path->content;
    	$sub = View::make('content.main')->with('content', $content);
    	return View::make('layouts.main')->with('content', $sub);

    }
    catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
    {
    	return Response::view('errors.404', array(), 404);
    }

})->where('all', '.*');

<?php

class GroupController extends BaseController {
	protected $layout = "layouts.main";

	public function getIndex() {

		$groups = Sentry::findAllGroups();

		$content = '';
		$modal_content = '';
		$extra_js = array();


		if(sizeof($groups) > 0) {
			$content .= '<table class="table table-striped"><tbody>';
			$extra_js[] = 'js/get_permissions.js';

			foreach($groups as $g) {
				$content .= '<tr>';
				$content .= '<td class="group-title" id="group-title-' . $g->id . '">' . e($g->name) . '</td>';
				$content .= '<td><button id="group-' . $g->id . '" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#adminModal">View permissions</button> | <a href="/admin/groups/'.$g->id.'/permissions/edit">Change permissions</a></td>';
				$content .= '<td><a href="/admin/groups/' . $g->id . '/edit">Edit</a> | <a href="/admin/groups/' . $g->id . '/delete">Delete</a></td>';
				$content .= '</tr>';

				// set up modal content
				try
				{
					$perms = $g->getPermissions();
					if(sizeof($perms) > 0) {
						$modal_content .= '<ul>';
						foreach ($perms as $key => $value) {
							$modal_content .= '<li>'. e($key) .'</li>';
						}
						$modal_content .= '</ul>';
					}
					else {
						$modal_content = 'No permissions for this group.';
					}
				}
				catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
				{
    				$modal_content = 'Group does not exist.';
				}
			}

			$content .= '</tbody></table>';
		}
		else {
			$content .= '<p>No groups.</p>';
		}

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Groups',
            'content' => $content,
            'modal' => true,
            'modal_title' => 'Permissions',
            'modal_content' => $modal_content,
        ));
        $this->layout->extra_js =  $extra_js;
	}

	public function getAdd() {

		$form  = Form::open(array('url' => '/admin/groups/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('group_name', 'Group name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('group_name', '', array('class' => 'form-control', 'placeholder' => 'Enter a group name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add Group',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function postAdd() {
		$error = false;
		try
		{
		    // Create the group
		    $group = Sentry::createGroup(array(
		        'name'        => Input::get('group_name'),
		    ));

		    return Redirect::to('admin')->with('message', 'Group has been created.');
		}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
		{
		    $error = 'Name field is required';
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
		    $error = 'Group already exists';
		}

		return Redirect::to('admin/groups/add')->with('message', $error);

	}

	public function getEdit(Cartalyst\Sentry\Groups\Eloquent\Group $group) {
    	$form  = Form::open(array('url' => '/admin/groups/'.$group->id.'/edit', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('group_name', 'Group name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('group_name', e($group->name), array('class' => 'form-control', 'placeholder' => 'New Group name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Edit Group',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postEdit(Cartalyst\Sentry\Groups\Eloquent\Group $group) {
    	$error = false;
    	try
		{
		    // Update the group details
		    $group->name = Input::get('group_name');

		    // Update the group
		    if ($group->save())
		    {
		        // Group information was updated
		        return Redirect::to('admin/groups')->with('message', 'Group has been updated');
		    }
		    else
		    {
		        // Group information was not updated
		        return Redirect::to('admin/groups')->with('message', 'Group was not updated');
		    }
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
		    $error = 'Group already exists.';
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    $error = 'Group was not found.';
		}
		return Redirect::to('admin/groups/'.$group->id.'/edit')->with('message', $error);
    }

	public function getDelete(Cartalyst\Sentry\Groups\Eloquent\Group $group) {
    	$group->delete();
    	return Redirect::to('/admin/groups')->with('message', 'Group has been deleted.');
    }

    // permissions
    public function getEditPermissionsForGroup(Cartalyst\Sentry\Groups\Eloquent\Group $group) {
    	try
    	{
    		$p = $default_perms = Letter::getConfig();
    		$default_perms = $p['default_permissions'];
    		$perms = $group->getPermissions();
    		$form  = Form::open(array('url' => '/admin/groups/'.$group->id.'/permissions/edit', 'method' => 'post'));
	        //$form
	        foreach($default_perms as $key => $p) {
	        	$checked = '';
	        	if(array_key_exists($key, $perms)) {
	        		$checked = 'checked="checked"';
	        	}
	        	$form .= '<div class="checkbox"><label>';
	        	$form .= '<input type="checkbox" name="permissions['. e($key) .']" value="' . e($p) . '" ' . $checked . '>' . e($key);
	        	$form .= '</label></div>';
	        }
	        $form .= '<div class="form-group">';
	        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
	        $form .= '</div>';
	        $form .= Form::close();

	        $content = $form;
    	}
    	catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
    	{
    		$content = 'Group was not found.';
    	}

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Edit Group',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postEditPermissionsForGroup(Cartalyst\Sentry\Groups\Eloquent\Group $group) {
    	$perms = Input::get('permissions');
    	// get the defaults
    	$p = $default_perms = Letter::getConfig();
    	$default_perms = $p['default_permissions'];
    	$error = false;
    	$new_perms = array();
    	if(isset($perms)) {
    		foreach ($perms as $key => $perm) {
    			$new_perms[ $key ] = 1;
    		}
    		foreach($default_perms as $key => $val) {
				if(!array_key_exists($key, $new_perms)) {
					$new_perms[ $key ] = 0;
				}
			}
		}
    	else {
    		foreach($default_perms as $key => $val) {
				if(!array_key_exists($key, $new_perms)) {
					$new_perms[ $key ] = 0;
				}
			}
    	}

		$group->permissions  = $new_perms;
		try {
			//error_log(print_r($group, true));
			if($group->save())
			{
				return Redirect::to('/admin/groups')->with('message', 'Permissions saved.');
			}
			else {
				return Redirect::to('/admin/groups')->with('message', 'Permissions not saved.');
			}
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
		    $error = 'Group already exists.';
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    $error = 'Group was not found.';
		}
		return Redirect::to('/admin/groups')->with('message', $error);
    }

    public function getPermissionsForGroup(Cartalyst\Sentry\Groups\Eloquent\Group $group)
    {
        $modal_content = '';
        try
        {
            $perms = $group->getPermissions();
            if(sizeof($perms) > 0) {
                $modal_content .= '<ul>';
                foreach ($perms as $key => $value) {
                    $modal_content .= '<li>'. e($key) .'</li>';
                }
                $modal_content .= '</ul>';
            }
            else {
                $modal_content = 'No permissions for this group.';
            }
            return $modal_content;
        }
        catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
        {
            $modal_content = 'Group does not exist.';
        }

    }

}

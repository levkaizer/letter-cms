<?php

class CategoryController extends BaseController {
    protected $layout = "layouts.main";

    public function getIndex() {
		$content = '';

		$cats = Category::all();
		if(sizeof($cats)) {
			$content .= '<table class="table table-striped"><tbody>';
			foreach($cats as $c) {
				$content .= '<tr>';
				$content .= '<td>'.HTML::link('/category/' .$c->id, $c->name).'</td>';
				$content .= '<td>'.HTML::link('/admin/category/' . $c->id . '/edit', 'Edit').'</td>';
				$content .= '<td>'.HTML::link('/admin/category/' . $c->id . '/delete', 'Delete').'</td>';
				$content .= '</tr>';
			}
			$content .= '</tbody></table>';
		}
		else {
			$content .= '<p>There are no categories to display.</p>';
			$content .= '<p><a href="/admin/category/add">Add one.</a>';
		}

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Manage categories',
            'content' => $content,
            'modal' => false,
        ));
	}

    public function getAdd() {

		$form  = Form::open(array('url' => '/admin/category/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('name', 'Category name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Enter a category name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add Category',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function postAdd() {
	    $category = new Category();
	    $category->name = Input::get('name');
	    $category->weight = 0;

	    $validator = Validator::make(Input::all(), Category::$rules);
	    if($validator->passes()) {
	        $category->save();
	        return Redirect::to('admin')->with('message', 'New category added.');
	    }

	    return Redirect::back()->withErrors($validator)->withInput();

	}

	public function getEdit(Category $category) {

		$form  = Form::open(array('url' => '/admin/category/'.$category->id.'/edit', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('name', 'Category name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('name', $category->name, array('class' => 'form-control', 'placeholder' => 'Enter a category name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add Category',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function postEdit(Category $category) {
	    $category->name = Input::get('name');
	    $validator = Validator::make(Input::all(), Category::$rules);
	    if($validator->passes()) {
	        $category->save();
	        return Redirect::to('admin/category')->with('message', 'Category updated.');
	    }

	    return Redirect::back()->withErrors($validator)->withInput();
	}

	public function getDelete(Category $category) {
	    $category->delete();
	    return Redirect::to('/admin/category')->with('message', 'Category has been removed.');
	}

	public function getList(Category $category) {
	    // build admin links
	    $content = '';
	    $terms = $category->terms;
	    if(sizeof($terms)) {
	    	$content .= '<table class="table table-striped"><tbody>';
			foreach($terms as $term) {
				$content .= '<tr>';
				$content .= '<td>'.HTML::link('category/term/' .$term->id, $term->name).'</td>';
				$content .= '<td>'.HTML::link('/admin/category/term/' . $term->id . '/edit', 'Edit').'</td>';
				$content .= '<td>'.HTML::link('/admin/category/term/' . $term->id . '/delete', 'Delete').'</td>';
				$content .= '</tr>';
			}
			$content .= '</tbody></table>';
	    }
	    else {
	    	$content .= '<p>There are no terms.</p>';
	    }

	    $content .= '<div>'.HTML::link('/admin/category/'.$category->id.'/add', 'Add term');

	    $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Views terms of categroy "'.$category->name.'"',
            'content' => $content,
            'modal' => false,
        ));

	}

	public function getTaggedPages(Category $category) {
	    $c = '';

	    $pages = $category->posts;

	    if(sizeof($pages)) {
	        $c .= '<table class="table table-striped"><tbody>';
			foreach($pages as $p) {
				$c .= '<tr>';
				$c .= '<td>'.HTML::link('content/' .$p->id, $p->title).'</td>';
				$c .= '</tr>';
			}
			$c .= '</tbody></table>';
	    }
	    else {
	       $conf = Letter::getConfig();
	       $c =  '<p>'.  $conf['no_posts_msg'] . '</p>';
	    }

	    $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Posts tagged with "'. $category->name .'"',
            'content' => $c,
            'modal' => false,
        ));
	}
}

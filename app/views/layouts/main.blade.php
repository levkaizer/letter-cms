<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{{ $letter->config['site_name'] }}}</title>
    {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/main.css') }}
    @if(isset($extra_css))
		@if(sizeof($extra_css))
			@foreach($extra_css as $css)
			{{ HTML::sytle($css) }}
			@endforeach
		@endif
	@endif

    {{ HTML::script('js/jquery-1.11.0.min.js') }}
    {{ HTML::script('packages/bootstrap/js/bootstrap.min.js') }}
    @if(isset($extra_js))
		@if(sizeof($extra_js))
			@foreach($extra_js as $js)
			{{ HTML::script($js) }}
			@endforeach
		@endif
	@endif
  </head>

  <body>

   <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          @if(!Sentry::check())
            <a class="navbar-brand" href="/">{{{ $letter->config['site_name'] }}}</a>
          @else
            <a class="navbar-brand" href="/admin">{{{ $letter->config['site_name'] }}}</a>
          @endif
        </div>
        <div class="collapse navbar-collapse">
          {{ Letter::menu() }}
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             @if(Sentry::check())
             {{ Letter::userLoggedInMenu() }}
             @else
             {{ Letter::userMenu() }}
             @endif
          </div>

        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      @if(Session::has('message'))
         <p class="alert alert-info">{{ Session::get('message') }}</p>
      @endif
      @if(Session::has('errors'))
         @foreach($errors->all() as $error)
         <p>{{{ $error }}}</p>
         @endforeach
      @endif
    </div>
   {{ $content }}
   <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand">&copy; {{ date('Y') }} LevelKurve</a>
        </div>
      </div>
   </nav>
  </body>
</html>

<?php

class Path extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'content_path';

	public static $rules = array(
        'path'=>'required|alpha_dash|unique:content_path|not_in:admin,user,profile',
    );

	public function content()
    {
        return $this->belongsTo('Content');
    }
}

<?php

class UserController extends BaseController {
    protected $layout = "layouts.main";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

    public function getRegister() {
        if(!Sentry::check()) {
            $this->layout->content = View::make('user.register');
        }
        else {
            return Redirect::to('admin');
        }
    }

    public function postRegister() {
        $error = false;

        try {
            $user = Sentry::createUser(array(
                'email'      => Input::get('email'),
                'password'   => Input::get('password'),
                'first_name' => Input::get('first_name'),
                'last_name'  => Input::get('last_name'),
                'activated'  => true,
            ));

            $group = Sentry::findGroupByName('Users');
            $user->addGroup($group);

            return Redirect::to('user/login')->with('message', 'Thanks for registering!');
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $error = 'Login field is required.';
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $error ='Password field is required.';
        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            $error = 'User with this login already exists.';
        }

        catch (Exception $e) {
            $error = $e->getMessage();
        }

        return Redirect::to('user/register')->with('message', 'The following errors occurred')->withErrors($error);
    }

    public function getLogin() {
        if(!Sentry::check()) {
            $this->layout->content = View::make('user.login');
        }
        else {
            // ther user is logged in, but cannot see the admin section
            $user = Sentry::getUser();
            if(!$user->hasAccess('access administration pages')) {
                return Redirect::guest('/')->with('message', 'You are not allowed in.');
            }
            return Redirect::to('admin');
        }
    }

    public function postLogin() {
        $error = false;
        try
        {
            $user = array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
            );
            if (Sentry::authenticate( $user, false))
            {
                return Redirect::intended('admin');
            }
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $error = 'Login field is required.';
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $error = 'Password field is required.';
        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            $error = 'Wrong username / password combination, try again.';
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            $error = 'User was not found.';
        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            $error = 'User is not activated.';
        }
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            $error = 'This user has been suspended.';
        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            $error = 'This user has been banned.';
        }

        return Redirect::to('user/login')->with('message', $error);
    }


    public function getLogout() {
        Sentry::logout();
        return Redirect::to('/user/login')->with('message', 'Your are now logged out!');
    }

    /** Admin user settings */
    public function getAdminListUsers() {
        $users = Sentry::findAllUsers();
        $content = 'No users.';
        if(sizeof($users) > 0) {
            $content  = '<table class="table table-striped"><tbody>';
            foreach($users as $u) {
                $content .= '<tr>';
                $content .= '<td>' . e($u->email) . '</td>';
                $content .= '<td>' . HTML::link('/admin/users/'. $u->id .'/edit', 'Edit') . '</td>';
                $content .= '<td>' . HTML::link('/admin/users/'. $u->id .'/delete', 'Delete') . '</td>';
                $content .= '</tr>';
            }
            $content .= '</tbody></table>';
        }
        $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'List Users',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function getAdminAdd() {
        $groups = Sentry::findAllGroups();
        $form  = Form::open(array('url' => '/admin/users/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('email', 'Email address', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Enter an email address') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= Form::label('password', 'Password', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::password('password', '', array('class' => 'form-control') );
        $form .= '</div></div>';

        if(sizeof($groups) > 0) {
            $form .= '<div class="form-group">';
            $form .= '<h3>Groups</h3>';
            foreach ($groups as $key => $g) {
                $form .= '<div class="checkbox"><label>';
                $form .= Form::checkbox('groups['.$g->name.']', $g->id) . $g->name;
                $form .= '</label></div>';
            }
            $form .= '</div>';
        }

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;
        $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add a User',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postAdminAdd() {
    	$current_user = Letter::getUser();
    	$action = false;
    	if($current_user->hasAccess('administer users')) {
    		$action = true;
    	}
    	if($action) {
			$error = false;

			try {
				$user = Sentry::createUser(array(
					'email'    => Input::get('email'),
					'password' => Input::get('password'),
					'activated' => true,
				));
				// now add groups
				if(null !==Input::get('groups')) {
					$groups = Input::get('groups');
					if(sizeof($groups)) {
						foreach($groups as $gid => $g) {
							$user->addGroup(Sentry::findGroupById($g));
						}
					}
				}

				return Redirect::to('/admin/users')->with('message', 'User added.');
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				$error = 'Login field is required.';
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				$error ='Password field is required.';
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				$error = 'User with this login already exists.';
			}

			catch (Exception $e) {
				$error = $e->getMessage();
			}
			return Redirect::back()->with('message', $error);
        }
        else {
        	return Redirect::back()->with('message', 'You are not authorized to perform this action.');
        }
    }

    public function getAdminEdit(Cartalyst\Sentry\Users\Eloquent\User $user) {
        $groups = Sentry::findAllGroups();
        $form  = Form::open(array('url' => '/admin/users/'.$user->id.'/edit', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('email', 'Email address', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('email', $user->email, array('class' => 'form-control', 'placeholder' => 'Enter an email address') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= Form::label('password', 'Password', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('password', '', array('class' => 'form-control', 'placeholder' => 'Enter a password') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('confirm_password', 'Confirm Password', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('confirm_password', '', array('class' => 'form-control', 'placeholder' => 'Confirm password') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('first_name', 'First name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('first_name', $user->first_name, array('class' => 'form-control', 'placeholder' => 'Enter your first name') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('last_name', 'Last name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('last_name', $user->last_name, array('class' => 'form-control', 'placeholder' => 'Enter your last name') );
        $form .= '</div></div>';

        $my_groups = $user->getGroups();
        if(sizeof($groups) > 0) {
            $form .= '<div class="form-group">';
            $form .= '<h3>Groups</h3>';
            foreach ($groups as $key => $g) {
                $checked = false;

                foreach($my_groups as $mg) {
                    if($mg->id == $g->id) {
                        $checked = true;
                        break;
                    }
                }
                $form .= '<div class="checkbox"><label>';
                $form .= Form::checkbox('groups['.$g->name.']', $g->id, $checked) . $g->name;
                $form .= '</label></div>';
            }
            $form .= '</div>';
        }

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;
        $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Add a User',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postAdminEdit(Cartalyst\Sentry\Users\Eloquent\User $user) {
    	$current_user = Letter::getUser();
    	$action = false;
    	if($current_user->hasAccess('administer users')) {
    		$action = true;
    	}
    	if($action) {
			// save the user details first
			$user->email = Input::get('email');
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			if(strlen(Input::get('password')) > 0 ) {
				if(Input::get('password') == Input::get('confirm_password')) {
					$user->passowrd = Input::get('password');
				}
			}
			$user->save();

			// now modify groups
			// If no groups were set, this will remove all groups.
			$groups = Input::get('groups');
			$system_groups = Sentry::findAllGroups();
			foreach ($system_groups as $key => $g) {
				// first remove groups
				$user->removeGroup(Sentry::findGroupById($g->id));
				if(isset($groups)) {
					if(in_array($g->id, $groups)) {
						$user->addGroup(Sentry::findGroupById($g->id));
					}
				}
			}
			return Redirect::to('/admin/users/'.$user->id.'/edit')->with('message', 'User updated.');
        }
        else {
        	return Redirect::to('/admin/users/'.$user->id.'/edit')->with('message', 'You are not authorized to perform this action.');
        }
    }

    public function getAdminDelete(Cartalyst\Sentry\Users\Eloquent\User $user) {
    	$delete = false;
    	$current_user = Letter::getUser();
    	if($user->id == 1) {
    		if($current_user->id == 1) {
    			$delete = true;
    		}
    	}

    	if($current_user->hasAccess('administer users')) {
    		$delete = true;
    	}

    	if($delete) {
    		$user->delete();
    		return Redirect::to('/admin/users')->with('message', 'User removed.');
    	}
    	else {
    		return Redirect::to('/admin/users')->with('message', 'You are not authorized to remove this user.');
    	}
    }
}

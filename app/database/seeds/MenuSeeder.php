<?php

class MenuSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	    DB::table('menu')->delete();

        Menu::create(array(
            'name' => 'Primary Menu',
        ));
        Menu::create(array(
            'name' => 'User menu',
        ));
        Menu::create(array(
            'name' => 'User menu (Logged in)',
        ));
	}

}

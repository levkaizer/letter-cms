<?php namespace Levelkurve\Lettercms\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class MenuItem extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'menu_items';
	protected $fillable = array('title', 'path', 'menu_id', 'parent', 'weight', 'active');
	
	public function menu() {
		return $this->belongsTo('\Levelkurve\Lettercms\Models\Menu');
	}
}

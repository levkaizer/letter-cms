<?php
class AdminController extends BaseController {
	protected $layout = "layouts.main";

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
    }

	public function getIndex() {
        $this->layout->content = View::make('user.admin')->with('user', Sentry::getUser());
    }

    public function getSiteSettings() {
    	$config = Letter::getConfig();
        $settings = array(
            'site_name' => ( null !== Setting::where('name', '=', 'site_name')->first() ? unserialize( Setting::where('name', '=', 'site_name')->first()->value ) : e($config['site_name']) ),
            'no_posts_msg' => ( null !== Setting::where('name', '=', 'no_posts_msg')->first() ? unserialize( Setting::where('name', '=', 'no_posts_msg')->first()->value ) : e($config['no_posts_msg']) ),
        );

        $form  = Form::open(array('url' => '/admin/site-settings', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('site_name', 'Site name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('site_name', $settings['site_name'], array('class' => 'form-control', 'placeholder' => 'Site name') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('no_posts_msg', '"No Posts" message', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('no_posts_msg', $settings['no_posts_msg'], array('class' => 'form-control', 'placeholder' => '"No Posts" message') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

        $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Site Settings',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postSiteSettings() {
        $settings = Input::all();

        foreach($settings as $key => $s) {

            if( !starts_with($key, '_') ) {
                $setting = Setting::firstOrCreate(array(
                    'name'  => $key));
                $setting->value = serialize($s);
                $setting->save();
            }
        }

        return Redirect::to('/admin/site-settings')->with('message', 'Settings have been saved');
    }

    public function getErrorPages() {

    	$config = Letter::getConfig();
        $settings = array(
            'not_found_msg' => ( null !== Setting::where('name', '=', 'not_found_msg')->first() ? unserialize( Setting::where('name', '=', 'not_found_msg')->first()->value ) : e($config['not_found_msg']) ),
           	'access_denied_msg' => ( null !== Setting::where('name', '=', 'access_denied_msg')->first() ? unserialize( Setting::where('name', '=', 'access_denied_msg')->first()->value ) : e($config['access_denied_msg']) ),
        );

        $form  = Form::open(array('url' => '/admin/error-pages', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('not_found_msg', 'Default "Page not found" message', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('not_found_msg', $settings['not_found_msg'], array('class' => 'form-control', 'placeholder' => 'Default "Page not found" message') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('access_denied_msg', 'Default "Access denied" message', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('access_denied_msg', $settings['access_denied_msg'], array('class' => 'form-control', 'placeholder' => 'Default "Access denied" message') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;

        $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Error Page Settings',
            'content' => $content,
            'modal' => false,
        ));
    }

    public function postErrorPages() {
        $settings = Input::all();

        foreach($settings as $key => $s) {

            if( !starts_with($key, '_') ) {
                $setting = Setting::firstOrCreate(array(
                    'name'  => $key));
                $setting->value = serialize($s);
                $setting->save();
            }
        }

        return Redirect::to('/admin/error-pages')->with('message', 'Settings have been saved');
    }
}

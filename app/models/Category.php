<?php

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';
	public static $rules = array(
        'name'=>'required|unique:categories',
    );

    public function terms()
    {
        return $this->hasMany('Term');
    }

    public function posts() {
        return $this->belongsToMany('Content');
    }

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth.admin.menu', 'prefix' => 'admin/menu'), function() {
	Route::model('menu', 'Levelkurve\Lettercms\Models\Menu');
	Route::model('menuItem', 'Levelkurve\Lettercms\Models\MenuItem');

	Route::get('settings', array('as' => 'admin.menu.getIndex', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@getMenuAdmin'));
	Route::post('settings', array('as' => 'admin.menu.getIndex', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@postMenuAdmin'));

	Route::get('/', array('as' => 'admin.menu.getIndex', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@getIndex'));

    Route::get('add', array('as' => 'admin.menu.getAdd', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@getAdd'));
    Route::post('add', array('as' => 'admin.menu.postAdd', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@postAdd'));

    Route::get('{menu}/delete', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuController@getDelete'));
    
    Route::get('{menu}', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@getMenuItems'));
    
    Route::get('{menu}/add', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@getAdd'));
    Route::post('{menu}/add', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@postAdd'));
    
    Route::get('{menu}/{menuItem}/edit', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@getEdit'));
    Route::post('{menu}/{menuItem}/edit', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@postEdit'));
    
    Route::get('{menu}/{menuItem}/delete', array('as' => 'admin.menu.getDelete', 'uses' => 'Levelkurve\Lettercms\Controllers\MenuItemController@getDelete'));

});

<?php

class MenuItemSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	    DB::table('menu_items')->insert(
	        array(
	            array(
                    'title'   => 'Home',
                    'path'    => '/',
                    'menu_id' => 1,
                    'weight'  => -10,
                    'parent'  => 0,
                    'active'  =>1,
                ),

                array(
                    'title'   => 'About',
                    'path'    => '/about',
                    'menu_id' => 1,
                    'weight'  => -9,
                    'parent'  => 0,
                    'active'  =>1,
                ),

                array(
                    'title'   => 'Contact',
                    'path'    => '/about/contact',
                    'menu_id' => 1,
                    'weight'  => -1,
                    'parent'  => 1,
                    'active'  =>1,
                ),

                array(
                    'title'   => 'Portfolio',
                    'path'    => '/about/portfolio',
                    'menu_id' => 1,
                    'weight'  => 0,
                    'parent'  => 1,
                    'active'  =>1,
                ),
                array(
                    'title'   => 'Login',
                    'path'    => '/user/login',
                    'menu_id' => 2,
                    'weight'  => -1,
                    'parent'  => 0,
                    'active'  =>1,
                ),
                array(
                    'title'   => 'Register',
                    'path'    => '/user/register',
                    'menu_id' => 2,
                    'weight'  => 0,
                    'parent'  => 0,
                    'active'  =>1,
                ),
                array(
                    'title'   => 'Logout',
                    'path'    => '/user/logout',
                    'menu_id' => 3,
                    'weight'  => 0,
                    'parent'  => 0,
                    'active'  =>1,
                ),
                array(
                    'title'   => 'Profile',
                    'path'    => '/user/profile',
                    'menu_id' => 3,
                    'weight'  => -1,
                    'parent'  => 0,
                    'active'  =>1,
                ),
	        )
	    );
	}

}

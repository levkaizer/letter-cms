<?php namespace Levelkurve\Lettercms\Controllers;
use \Levelkurve\Lettercms\Models\Menu as Menu;
use \Levelkurve\Lettercms\Models\MenuItem as MenuItem;

class MenuItemController extends \BaseController {
	protected $layout = "layouts.main";

	public function getMenuItems(Menu $menu)
	{
		$items = \LKMenu::build($menu->id, 1);
		/*
		print '<pre>';
		print_r($items);
		die;
		*/
		$content = '';
		if(sizeof($items)) {
			$content = '<table class="table table-striped">';
			$content .= '<tbody>';
			foreach($items as $i) {
				$content .= '<tr>';
				$content .= '<td>'.e($i->title).'</td>';
				$content .= '<td>'.\HTML::link('admin/menu/'.$menu->id.'/'.$i->id.'/edit', 'Edit').'</td>';
				$content .= '<td>'.\HTML::link('admin/menu/'.$menu->id.'/'.$i->id.'/delete', 'Delete').'</td>';
				$content .= '</tr>';
				if(isset($i->children)) {
					foreach($i->children as $child) {
						$content .= '<tr>';
						$content .= '<td>&nbsp;&nbsp;&nbsp;'.e($child->title).'</td>';
						$content .= '<td>'.\HTML::link('admin/menu/'.$menu->id.'/'.$child->id.'/edit', 'Edit').'</td>';
						$content .= '<td>'.\HTML::link('admin/menu/'.$menu->id.'/'.$child->id.'/delete', 'Delete').'</td>';
						$content .= '</tr>';
					}
				}
			}
			$content .= '</tbody></table>';
			$content .= '<p>'.\HTML::link('admin/menu/'.$menu->id.'/add', 'Add item').'</p>';

		}
		else {
			$content = '<p>There are no menu items for this menu.</p>';
		}
		$this->layout->content = \View::make('site.admin-page')->with(array(
            'page_title' => 'Manage menu "'. $menu->name .'"',
            'content' => $content,
            'modal' => false,
        ));

	}

	public function getAdd(Menu $menu)
	{
		$content = '';
		$this->layout->content = \View::make('site.admin-page')->with(array(
			'page_title' => 'Add item to menu "'. $menu->name .'"',
			'content' => $content,
			'modal' => false,
		));
	}

	public function postAdd(Menu $menu)
	{
	    $form  = \Form::open(array('url' => '/admin/menu/'.$menu->id.'/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= \Form::label('name', 'Menu name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= \Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Enter a menu name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= \Form::submit('Add menu', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= \Form::close();

        $content = $form;
        $this->layout->content = \View::make('site.admin-page')->with(array(
            'page_title' => 'Add item to "'. $menuItem->title .'"',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function getEdit(Menu $menu, Menuitem $menuItem)
	{
		if($menuItem->menu_id == $menu->id) {
		    $form  = \Form::open(array('url' => '/admin/menu/'.$menu->id.'/'.$menuItem->id.'/edit', 'method' => 'post', 'class' =>'form-horizontal'));
            //$form
            $form .= '<div class="form-group">';
            $form .= \Form::label('name', 'Menu name', array('class' => 'col-sm-2 control-label'));
            $form .= '<div class="col-sm-10">';
            $form .= \Form::text('name', $menuItem->title, array('class' => 'form-control', 'placeholder' => 'Enter a menu name') );
            $form .= '</div></div>';
            $form .= '<div class="form-group">';
            $form .= '<div class="col-sm-offset-2 col-sm-10">';
            $form .= \Form::submit('Add menu', array('class'=>'btn btn-large btn-success btn-block'));
            $form .= '</div></div>';
            $form .= \Form::close();

			$content = $form;
			$this->layout->content = \View::make('site.admin-page')->with(array(
				'page_title' => 'Edit "'. $menuItem->title .'"',
				'content' => $content,
				'modal' => false,
			));
		}
		else {
			return \Redirect::to('admin/menu')->with('message', 'Illegal choice detected.');
		}
	}

	public function postEdit(Menu $menu, Menuitem $menuItem)
	{

	}

	public function getDelete(Menu $menu, Menuitem $menuItem)
	{
		if($menuItem->menu_id == $menu->id) {
			$menuItem->delete();
			return \Redirect::to('admin/menu')->with('message', 'Menu item removed.');
		}
		else {
			return \Redirect::to('admin/menu')->with('message', 'Illegal choice detected.');
		}
	}

}

<?php namespace Levelkurve\Lettercms;

use Illuminate\Support\ServiceProvider;
use \Cartalyst\Sentry\Facades\Laravel\Sentry as Sentry;
use \Setting as Setting;

class LettercmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('levelkurve/lettercms');
		require __DIR__.'/../../routes.php';
		require __DIR__.'/../../filters.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		/**
		 * Set up the default variables
		 */
		\App::singleton('Letter', function(){
			$app = new Letter();

			// load basic settings
			if(null !== \Setting::where('name', '=', 'site_name')->first()) {
				$site_name = unserialize( \Setting::where('name', '=', 'site_name')->first()->value );
			}
			else {
				$site_name = 'Letter CMS';
			}

			// menus
			if(null !== \Setting::where('name', '=', 'default_menu')->first()) {
				$default_menu = unserialize( \Setting::where('name', '=', 'default_menu')->first()->value );
			}
			else {
				$default_menu = 1;
			}

			if(null !== \Setting::where('name', '=', 'default_user_menu')->first()) {
				$default_user_menu = unserialize( \Setting::where('name', '=', 'default_user_menu')->first()->value );
			}
			else {
				$default_user_menu = 2;
			}

			if(null !== \Setting::where('name', '=', 'default_user_logged_in_menu')->first()) {
				$default_user_logged_in_menu = unserialize( \Setting::where('name', '=', 'default_user_logged_in_menu')->first()->value );
			}
			else {
				$default_user_logged_in_menu = 3;
			}

			// set the default 404 message
			if(null !== \Setting::where('name', '=', 'not_found_msg')->first()) {
				$not_found_msg = unserialize( \Setting::where('name', '=', 'not_found_msg')->first()->value );
			}
			else {
				$not_found_msg = 'Page not found.';
			}

			// set the default 403 message
			if(null !== \Setting::where('name', '=', 'access_denied_msg')->first()) {
				$access_denied_msg = unserialize( \Setting::where('name', '=', 'access_denied_msg')->first()->value );
			}
			else {
				$access_denied_msg = 'You are not authorized to access this content.';
			}

			// set the default No Posts message
			if(null !== \Setting::where('name', '=', 'no_posts_msg')->first()) {
				$no_posts_msg = unserialize( \Setting::where('name', '=', 'no_posts_msg')->first()->value );
			}
			else {
				$no_posts_msg = 'There are currently no posts to display.';
			}

			// set the default permissions
			$default_permissions = array(
				'access administration pages' => 1,
				'administer permissions' => 1,
				'administer users' => 1,
				'administer settings' => 1,
				'administer content' => 1,
				'administer categories' => 1,
				'administer menus' => 1,
				'create content' => 1,
				'access content' => 1,
			);

			$app->config = array(
				'site_name' => $site_name,
				'no_posts_msg' => $no_posts_msg,
				'default_permissions' => $default_permissions,
				'not_found_msg' => $not_found_msg,
				'access_denied_msg' => $access_denied_msg,
				'default_menu' => $default_menu,
				'default_user_menu' => $default_user_menu,
				'default_user_logged_in_menu' => $default_user_logged_in_menu,
			);

			// set the default user
			$u = Sentry::getUser();
			if(!isset($u)) {
				$u = new \Cartalyst\Sentry\Users\Eloquent\User();
				$u->id = 0;
				$u->permissions = array('access content' => 1);
			}
			$app->user = $u;

			return $app;

		});

		\App::before(function($request){

 			$app = \App::make('Letter');
 			$m = new LKMenu();

			// set up groups
			try
			{
				Sentry::findGroupByName('Admin');
			}
			catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				$group = Sentry::createGroup(array(
	        		'name'        => 'Admin'
	    		));
	    		// now set up permissions
	    		$group = Sentry::findGroupByName('Admin');
	    		$group->permissions = $app->config['default_permissions'];
	    		$group->save();
			}
			try
			{
				Sentry::findGroupByName('Users');
			}
			catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				$group2 = Sentry::createGroup(array(
	        		'name'        => 'Users'
	    		));
	    		$group2->permissions = array('access content' => 1);
	    		$group2->save();
			}

			try
			{
				Sentry::findUserById(1);
			}
			catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				// no user, make one!
				Sentry::createUser(array(
					'email' => 'kev@kpbowler.co.uk',
					'password' => 'fma0207',
					'activated' => 1,
				));
				$user = Sentry::findUserById(1);
				$user->permissions = array('superuser' => 1);
				$user->addGroup(Sentry::findGroupByName('Admin'));
				$user->save();
			}

			$m->configure(
                array(
                    'ul_wrap' => null
                )
            );
            $menu = $m->render( $app->config['default_menu'] );
            $app->setMenu($menu);

            $m->configure(
                array(
                    'ul_wrap' => null,
                    'ul_class' => 'nav navbar-nav navbar-right ',
                )
            );
            if(\Sentry::check()) {

                $userLIMenu = $m->render( $app->config['default_user_logged_in_menu'] );
                $app->setUserLoggedInMenu($userLIMenu);
            }
            else {
                $userMenu = $m->render( $app->config['default_user_menu'] );
                $app->setUserMenu($userMenu);
            }

            \View::share('letter', $app);

		});


        $this->app['letter'] = $this->app->share(function($app)
        {
        	return \App::make('Letter');
        });

        $this->app['lkmenu'] = $this->app->share(function($app)
        {
        	return new LKMenu();
        });


        $this->app->booting(function() {
                    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
                    $loader->alias('Letter', 'Levelkurve\Lettercms\Facades\Letter');
                    $loader->alias('LKMenu', 'Levelkurve\Lettercms\Facades\LKMenu');
        });


	}


}

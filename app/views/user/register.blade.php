<div class="container" id="home-wrap">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <h2>Please Register</h2>
          {{ Form::open(array('url'=>'user/register', 'class'=>'form-signup form-horizontal')) }}

             @if(sizeof($errors))
              <ul>
                @foreach($errors->all() as $error)
                   <li>{{ $error }}</li>
                @endforeach
              </ul>
             @endif
             {{ Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
             {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
             {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
             {{ Form::text('first_name', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
             {{ Form::text('last_name', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
             {{ Form::submit('Register', array('class'=>'btn btn-large btn-success btn-block'))}}
          {{ Form::close() }}
        </div>
    </div>
</div>

<div class="container" id="home-wrap">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Please Login</h2>
            {{ Form::open(array('url'=>'user/login', 'class'=>'form-signin form-horizontal')) }}
                    {{ Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                    {{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
            {{ Form::close() }}
        </div>
    </div>
</div>

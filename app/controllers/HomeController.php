<?php

class HomeController extends BaseController {
	protected $layout = "layouts.main";

	public function getIndex()
	{
		// get all posts where the status is published, ordered by most recent.
		$conf = Letter::getConfig();
		$content = '';
		$posts = Content::where('status', '=', 1)->orderBy('created_at', 'desc')->get();
		if(sizeof($posts) > 0) {
			foreach($posts as $post) {
				$path = '/content/'.$post->id;
				if(isset($post->path->path)) {
					$path = '/'.$post->path->path;
				}

				$content .= '<article id="content-'.$post->id.'">';
				$content .= '<section class="article-content">';
				$content .= '<h3><a href="'.$path.'">'. e( $post->title ) .'</a></h3>';
				$content .= '<p>'. e( $post->teaser ) .'</p>';
				$content .= '</section>';
				$content .= '<section class="article-meta">';
				$content .= '<div clas="article-post-date">Posted: '. e( $post->created_at ) .'</p>';
				$content .= '</section>';
				$content .= '</article>';
			}
		}
		else {
			$content .= '<strong>'. $conf['no_posts_msg'] .'</strong>';
		}

		$this->layout->content = View::make('site.homepage')->with(array(
            'page_title' => e($conf['site_name']),
            'content' => $content,
        ));
	}

}
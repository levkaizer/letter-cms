<?php namespace Levelkurve\Lettercms\Controllers;
use \Levelkurve\Lettercms\Models\Menu as Menu;

class MenuController extends \BaseController {
	protected $layout = "layouts.main";

	public function getIndex() {
	    $menus = Menu::all();

	    $content = '';

	    if(sizeof($menus)) {
			$content .= '<table class="table table-striped"><tbody>';
			foreach($menus as $m) {
				$content .= '<tr>';
				$content .= '<td>'.\HTML::link('/admin/menu/' . $m->id, $m->name).'</td>';
				$content .= '<td>'.\HTML::link('/admin/menu/' . $m->id . '/edit', 'Edit').'</td>';
				$content .= '<td>'.\HTML::link('/admin/menu/' . $m->id . '/delete', 'Delete').'</td>';
				$content .= '</tr>';
			}
			$content .= '</tbody></table>';
		}
		else {
			$content .= '<p>There are no menus to display.</p>';
		}
		$content .= '<p><a href="/admin/menu/add">Add a menu.</a>';

		$this->layout->content = \View::make('site.admin-page')->with(array(
            'page_title' => 'Manage menus',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function getAdd() {
	    $form  = \Form::open(array('url' => '/admin/menu/add', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= \Form::label('name', 'Menu name', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= \Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Enter a menu name') );
        $form .= '</div></div>';
        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= \Form::submit('Add menu', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= \Form::close();

        $content = $form;

		$this->layout->content = \View::make('site.admin-page')->with(array(
            'page_title' => 'Add Menu',
            'content' => $content,
            'modal' => false,
        ));
	}
	public function postAdd() {
	    $menu = new Menu();
	    $menu->name = \Input::get('name');

	    $validator = \Validator::make(\Input::all(), Menu::$rules);
	    if($validator->passes()) {
	        $menu->save();
	        return \Redirect::to('admin')->with('message', 'New Menu added.');
	    }

	    return \Redirect::back()->withErrors($validator)->withInput();
	}

	public function getDelete(Menu $menu) {
	    $menu->delete();
	    return \Redirect::to('/admin/menu')->with('message', 'Menu has been removed.');
	}

	public function getMenuAdmin() {
	    $config = \Letter::getConfig();
        $settings = array(
            'default_menu' => ( null !== \Setting::where('name', '=', 'default_menu')->first() ? unserialize( \Setting::where('name', '=', 'default_menu')->first()->value ) : e($config['default_menu']) ),
        );

        // get all menus
        $all_menus = Menu::all();
        $menus = array();
        foreach($all_menus as $m) {
            $menus[ $m->id ] = $m->name;
        }

        // get selected cats
		$selected_menu = array();
        foreach($all_menus as $m) {
            if($m->id == $settings['default_menu']) {
                $selected_menu[ $m->id ] = $m->id;
                break;
            }
        }

        $form  = \Form::open(array('url' => '/admin/menu/settings', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= \Form::label('default_menu', 'Default manu', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= \Form::select('default_menu', $menus, $selected_menu);
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= \Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= \Form::close();

        $content = $form;

	    $this->layout->content = \View::make('site.admin-page')->with(array(
            'page_title' => 'Menu Settings',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function postMenuAdmin() {
	    $settings = \Input::all();

        foreach($settings as $key => $s) {

            if( !starts_with($key, '_') ) {
                $setting = \Setting::firstOrCreate(array(
                    'name'  => $key));
                $setting->value = serialize($s);
                $setting->save();
            }
        }
        return \Redirect::to('admin')->with('message', 'Menu settings updated.');

	}

}

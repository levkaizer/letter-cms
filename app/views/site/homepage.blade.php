<div class="container" id="homepage">
    <h2>{{{ $page_title }}}</h2>

    {{ $content }}

</div>
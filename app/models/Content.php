<?php

class Content extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'content';

	public function path()
    {
        return $this->hasOne('Path');
    }

    public function categories() {
        return $this->belongsToMany('Category');
    }

}

<?php

class ContentController extends BaseController {
	protected $layout = "layouts.main";

	public function getIndex() {
		$content = '';

		$co = Content::all();

		$content .= '<table class="table table-striped"><tbody>';
		foreach($co as $c) {
            $path = '/content/'.$c->id;
            if(strlen($c->path)) {
                $path = '/'.$c->path->path;
            }
		    $content .= '<tr>';
		    $content .= '<td>'.HTML::link($path, $c->title).'</td>';
		    $content .= '<td>'.HTML::link('/admin/content/' . $c->id . '/edit', 'Edit').'</td>';
		    $content .= '<td>'.HTML::link('/admin/content/' . $c->id . '/delete', 'Delete').'</td>';
		    $content .= '</tr>';
		}
		$content .= '</tbody></table>';

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Manage content',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function getCreate() {
		$content = '';

		// get all categories
		$categories = Category::all();
		$cats = array();
		foreach($categories as $c) {
		    $cats[ $c->id ] = $c->name;
		}

		$form  = Form::open(array('url' => '/admin/content/create', 'method' => 'post', 'class' =>'form-horizontal'));
        //$form
        $form .= '<div class="form-group">';
        $form .= Form::label('title', 'Title', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('title', Input::old('title'), array('class' => 'form-control', 'placeholder' => 'Enter a title') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('category[]', 'Category', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::select('category[]', $cats, array(), array('multiple'));
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('teaser', 'Teaser', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('teaser', Input::old('teaser'), array('class' => 'form-control', 'placeholder' => 'Enter a short intro') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('body', 'Body', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::textarea('body', Input::old('body'), array('class' => 'form-control') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('path', 'Path', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('path', Input::old('path'), array('class' => 'form-control', 'placeholder' => 'Enter a custom URL') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= '<h3>Status</h3>';
        $form .= '<div class="radio"><label>';
        $form .= Form::radio('status', '0', true) . 'Unpublished';
        $form .= '</label></div>';
        $form .= '<div class="radio"><label>';
        $form .= Form::radio('status', '1') . 'Published';
        $form .= '</label></div>';
        $form .= '</div>';

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';
        $form .= Form::close();

        $content = $form;
        $content .= HTML::script('ckeditor/ckeditor.js');
        $content .= "<script>CKEDITOR.replace( 'body' );</script>";

		$this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Create content',
            'content' => $content,
            'modal' => false,
        ));
	}

	public function postCreate() {
	    $user = Sentry::getUser();

	    $content = new Content();
	    $content->uid = $user->id;
	    $content->title = Input::get('title');
	    $content->teaser = Input::get('teaser');
	    $content->body = Input::get('body');
	    $content->status = Input::get('status');

        if(strlen(Input::get('path'))) {
            $new_path = Input::get('path');
            if($new_path[0] == '/') {
                $new_path = substr($new_path, 1);
            }
            $validator = Validator::make(Input::all(), Path::$rules);
            if ($validator->passes()) {
                // only save the content after URL has been set
                $content->save();
                $path = new Path();
                $path->path = e($new_path);
                $path->content_id = $content->id;
                $path->save();
            }
            else {
                return Redirect::back()->withErrors($validator)->withInput();
            }
        }
        else {
            $content->save();
        }

        // terms
	    $cats = Input::get('category');
	    if(is_array($cats) && sizeof($cats)) {
	        foreach($cats as $cat) {
	            $c = Category::find($cat);
	            $content->categories()->save($c);
	        }

	    }

	    return Redirect::to('/admin')->with('message', 'Content created.');
	}

	public function getEdit(Content $content) {
	    $c = '';

        $path = ( isset($content->path->path) ) ? $content->path->path : '';

        // get all categories
		$categories = Category::all();
		$cats = array();
		foreach($categories as $c) {
		    $cats[ $c->id ] = $c->name;
		}

		// get selected cats
		$selected_cats = array();
		if(sizeof($content->categories)) {
            foreach($content->categories as $c) {
                $selected_cats[ $c->id ] = $c->id;
            }
		}

	    $form = Form::model($content, array('route' => array('admin.content.postEdit', $content->id)));

	    $form .= '<div class="form-group">';
        $form .= Form::label('title', 'Title', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('title', $content->title, array('class' => 'form-control', 'placeholder' => 'Enter a title') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('category[]', 'Category', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::select('category[]', $cats, $selected_cats, array('multiple'));
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('teaser', 'Teaser', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('teaser', $content->teaser, array('class' => 'form-control', 'placeholder' => 'Enter a short intro') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('body', 'Body', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::textarea('body', $content->body, array('class' => 'form-control') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= Form::label('path', 'Path', array('class' => 'col-sm-2 control-label'));
        $form .= '<div class="col-sm-10">';
        $form .= Form::text('path', $path, array('class' => 'form-control', 'placeholder' => 'Enter a custom URL') );
        $form .= '</div></div>';

        $form .= '<div class="form-group">';
        $form .= '<h3>Status</h3>';
        $form .= '<div class="radio"><label>';
        $form .= Form::radio('status', '0', $content->status ) . 'Unpublished';
        $form .= '</label></div>';
        $form .= '<div class="radio"><label>';
        $form .= Form::radio('status', '1', !$content->status ) . 'Published';
        $form .= '</label></div>';
        $form .= '</div>';

        $form .= '<div class="form-group">';
        $form .= '<div class="col-sm-offset-2 col-sm-10">';
        $form .= Form::submit('Save', array('class'=>'btn btn-large btn-success btn-block'));
        $form .= '</div></div>';

	    $form .= Form::close();

	    $c = $form;
        $c .= HTML::script('ckeditor/ckeditor.js');
        $c .= "<script>CKEDITOR.replace( 'body' );</script>";

	    $this->layout->content = View::make('site.admin-page')->with(array(
            'page_title' => 'Create content',
            'content' => $c,
            'modal' => false,
        ));
	}

	public function postEdit(Content $content) {
	    $content->title = Input::get('title');
	    $content->teaser = Input::get('teaser');
	    $content->body = Input::get('body');
	    $content->status = Input::get('status');

	    $content->save();

	    // terms
	    $content->categories()->detach();
	    $cats = Input::get('category');
	    if(is_array($cats) && sizeof($cats)) {
	        foreach($cats as $cat) {
	            $c = Category::find($cat);
	            $content->categories()->save($c);
	        }

	    }

        if(strlen(Input::get('path'))) {
            $new_path = Input::get('path');
            $old_path = '';
            if(isset($content->path)) {
                $old_path = $content->path->path;
            }
            // Only act if the strings are different
            if(strcmp($new_path, $old_path) !== 0) {
                if($new_path[0] == '/') {
                    $new_path = substr($new_path, 1);
                }
                $validator = Validator::make(Input::all(), Path::$rules);
                if ($validator->passes()) {
                    if(isset($content->path->id)) {
                        $path = Path::where('id', '=', $content->path->id)->first();
                    }
                    else {
                        $path = new Path();
                    }

                    $path->path = e($new_path);
                    $path->content_id = $content->id;
                    $path->save();
                }
                else {
                    return Redirect::to('/admin/content/'. $content->id .'/edit')->with('message', 'Path must be unique');
                }
            }

        }

	    return Redirect::to('/admin/content')->with('message', 'Content has been updated.');
	}

	public function getDelete(Content $content) {
	    $content->delete();
	    return Redirect::to('/admin/content')->with('message', 'Content has been removed.');
	}

	public function getView(Content $content) {
	    $user = Letter::getUser();
	    $c = false;

	    // Do we have a user?
	    if(!Letter::isAnonymous()) {
	    	// now check permissions of the user
	    	if($user->hasAccess('administer content')) {
	    		$c = $content;
	    	}
	    }
	    // If the content HAS NOT been set, follow the usual rules.
	    if(!$c) {
	    	if($content->status) {
                $c = $content;
            }
            else {
                $conf = Letter::getConfig();
                $c = new stdClass();
                $c->title = 'Access denied';
                $c->body = $conf['access_denied_msg'];
            }
	    }

	    // remove any unwanted attributes.
	    $dom = new DOMDocument;
        $dom->loadHTML($c->body);
        $xpath = new DOMXPath($dom);
        $nodes = $xpath->query('//@*');
        foreach ($nodes as $node) {
            $node->parentNode->removeAttribute($node->nodeName);
        }

        $c->body = $dom->saveHTML();

	    // now, clean the HTML elements
	    $c->body = strip_tags($c->body, "<a><b><i><em><abbr><address><article><aside><bdi><bdo><br><button><caption><cite><code><dd><details><dfn><div><dl><dt><figcaption><figure><h1><h2><h3><h4><h5><h6><hr><img><legend><li><main><mark><menu><menuitem><ol><p><pre><q><section><samp><small><strong><sub><sup><table><tbody><td><tfoot><th><thead><tr><u><ul><wbr>");


	    $this->layout->content = View::make('content.main')->with(array(
            'content' => $c,
        ));
	}
}

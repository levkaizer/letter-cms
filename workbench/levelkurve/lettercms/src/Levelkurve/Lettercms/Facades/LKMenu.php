<?php namespace LevelKurve\Lettercms\Facades;

use Illuminate\Support\Facades\Facade;

class LKMenu extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'lkmenu'; }
}

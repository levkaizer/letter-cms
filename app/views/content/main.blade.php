<div class="container">
    <h1>{{{ $content->title }}}</h1>
    {{ $content->body }}
</div>

<div class="container" id="tags">
    @if(!is_null($content->categories))
        @if(sizeof($content->categories) )
            <h3>Tagged with:</h3>
            @foreach($content->categories as $cat)
                <a href="/category/{{ $cat->id }}">{{ $cat->name }}</a>
            @endforeach
        @endif
    @endif
</div>
